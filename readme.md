# yii2-sms_ru

[Yii2](http://www.yiiframework.com) extension for [sms.ru](http://sms.ru)

## Installation

### Composer

The preferred way to install this extension is through [Composer](http://getcomposer.org/).
